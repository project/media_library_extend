<?php

namespace Drupal\media_library_extend\Plugin\MediaLibrarySource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media library pane to pull placeholder images from lorem.picsum.
 *
 * @MediaLibrarySource(
 *   id = "lorem_picsum",
 *   label = @Translation("Lorem Picsum"),
 *   source_types = {
 *     "image",
 *   },
 * )
 */
class LoremPicsum extends MediaLibrarySourceBase {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('file_system'),
      $container->get('http_client'),
      $container->get('file.repository')
    );
  }

  /**
   * Constructs a new LoremPicsum object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   * @param \Drupal\file\FileRepositoryInterface|null $file_repository
   *   The file repository service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, Token $token, FileSystemInterface $file_system, Client $http_client, FileRepositoryInterface $file_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $token, $file_system);
    $this->httpClient = $http_client;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResults() {
    $response = $this->httpClient->request('GET', 'https://picsum.photos/v2/list', [
      'headers' => [
        // @todo Check if we need this header.
        'User-Agent' => 'Mozilla/5.0',
      ],
      'query' => [
        'page' => $this->getValue('page') + 1,
        'limit' => $this->configuration['items_per_page'],
      ],
    ]);

    // @todo Error handling.
    $images = json_decode((string) $response->getBody(), TRUE);
    $grayscale = $this->getValue('grayscale');

    $results = [];
    foreach ($images as $image) {
      $height = floor(200 * $image['height'] / $image['width']);

      $results[] = [
        'id' => $image['id'] . ($grayscale ? ':grayscale' : ''),
        'label' => $image['author'],
        'preview' => [
          '#type' => 'html_tag',
          '#tag' => 'img',
          '#attributes' => [
            'src' => 'https://picsum.photos/id/' . $image['id'] . '/200/' . $height . ($grayscale ? '?grayscale' : ''),
            'alt' => $image['author'],
            'title' => $image['author'],
          ],
        ],
      ];
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state) {
    $form['grayscale'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grayscale'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \JsonException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntityId($selected_id) {
    $id_parts = explode(':', $selected_id);
    $selected_id = $id_parts[0];

    $response = $this->httpClient->request('GET', 'https://picsum.photos/id/' . $selected_id . '/info', [
      'headers' => [
        // @todo Check if we need this header.
        'User-Agent' => 'Mozilla/5.0',
      ],
    ]);

    // @todo Error handling.
    $info = json_decode((string) $response->getBody(), TRUE);

    $grayscale = in_array('grayscale', $id_parts);
    $url = 'https://picsum.photos/id/' . $selected_id . '/' . $info['width'] . '/' . $info['height'] . ($grayscale ? '?grayscale' : '');

    // Create a media entity.
    $entity = $this->createEntityStub('Lorem Picsum - ' . $selected_id);

    // Download the requested file.
    try {
      $response = $this->httpClient->request('GET', $url, [
        'timeout' => 30,
      ]);

      if ($response->getStatusCode() != 200) {
        // @todo Error handling.
        return NULL;
      }

      // Get file extension from response, since the selected download profile
      // might use a different extension than the original file.
      $filename = $selected_id . '.jpg';

      // Save to filesystem.
      $file = $this->fileRepository->writeData($response->getBody(), $this->getUploadLocation() . '/' . $filename);

      // Attach file to media entity.
      $source_field = $this->getSourceField();
      $entity->{$source_field}->target_id = $file->id();
      $entity->{$source_field}->alt = $this->t('Placeholder image by @author', [
        '@author' => $info['author'],
      ]);
      $entity->save();

      return $entity->id();
    }
    catch (\Exception $e) {
      watchdog_exception('media_library_extend', $e);
    }
  }

}
